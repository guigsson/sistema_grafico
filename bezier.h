#ifndef BEZIER_H
#define BEZIER_H

#include "graphicobject.h"

class Bezier : public GraphicObject
{
public:
    Bezier(std::string*, std::list<Coordinate*>);
    ~Bezier();
    std::list<Coordinate*> getWorldCoordinates();
    std::list<Coordinate*> getNormalizedCoordinates();
    void setNormalizedCoordinates(std::list<Coordinate*> coordinates);
    std::list<Coordinate*> generateCurveCoordinates();
    Bezier *clone();

private:
    std::list<Coordinate*> coordinates;
    std::list<Coordinate*> normalized_coordinates;

};


#endif // BEZIER_H
