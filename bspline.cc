#include "bspline.h"


BSpline::BSpline(std::string* name, std::list<Coordinate*> coordinates) : GraphicObject(name, BSPLINE)
{
    this->name = name;
    this->coordinates = coordinates;
}

BSpline::~BSpline()
{

}

std::list<Coordinate*> BSpline::getWorldCoordinates()
{
    return this->coordinates;
}

std::list<Coordinate *> BSpline::getNormalizedCoordinates()
{
    return this->normalized_coordinates;
}

void BSpline::setNormalizedCoordinates(std::list<Coordinate *> coordinates)
{
    this->normalized_coordinates = coordinates;
}

BSpline *BSpline::clone()
{
    std::list<Coordinate*> coordClone;
    std::list<Coordinate*> normalizedClone;

    for (Coordinate *coordinate: this->coordinates) {
        coordClone.push_back(coordinate->clone());
    }	 	  	 	   	      	    		  	     	       	 	
    for (Coordinate *coordinate: this->normalized_coordinates) {
        normalizedClone.push_back(coordinate->clone());
    }

    BSpline *curve = new BSpline(this->name, coordClone);
    curve->setStrokeColor(this->getStrokeColor());
    curve->setNormalizedCoordinates(normalizedClone);

    return curve;
}

std::list<Coordinate*> BSpline::generateCurveCoordinates()
{
    std::list<Coordinate*> coords;

    if (this->coordinates.size() >= 4)
    {
        float acurracy = 0.01;

        std::list<Coordinate*>::iterator it = this->coordinates.begin();

        Coordinate* c1 = *(it);
        Coordinate* c2 = *(std::next(it, 1));
        Coordinate* c3 = *(std::next(it, 2));
        Coordinate* c4 = *(std::next(it, 3));

        for (float t = 0; t <= 1; t += acurracy)
        {

            double x,y;

            x = (1-t)*(1-t)*(1-t)*c1->getX()
                + 3*t*((1-t)*(1-t))*c2->getX()
                + 3*t*t*(1-t)*c3->getX()
                + t*t*t*c4->getX();

            y = (1-t)*(1-t)*(1-t)*c1->getY()
                + 3*t*((1-t)*(1-t)*c2->getY())
                + 3*t*t*(1-t)*c3->getY()
                + t*t*t*c4->getY();

            coords.push_back(new Coordinate(x, y, 0.0));
        }	 	  	 	   	      	    		  	     	       	 	
    }

    return coords;
}

