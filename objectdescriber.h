#ifndef OBJECTDESCRIBER_H
#define OBJECTDESCRIBER_H

#include <list>
#include <map>
#include <string>
#include <tuple>
#include "graphicobject.h"
#include "coordinate.h"

enum Token {
    VerticeDescr,
    NegInteger,
    Integer,
    Double,
    CurveTypeId,
    Curve2DObj,
    BezierCurve,
    BSplineCurve,
    PointObj,
    LineObj,
    PolygonObj,
    NameTag,
    ObjName,
    EndTag,
    FileEnd,
    Invalid
};

class ObjectDescriber
{
 public:
    ObjectDescriber();
    std::string toObj(std::list<GraphicObject*> objects);
    std::list<GraphicObject*> fromObj(std::string obj);

 private:
    void tokenize(std::string obj_str);
    std::pair<Token, std::string> getNextToken();
    std::pair<Token, std::string> peekNextToken(int offset);

    Token interpretToken(std::string token);

    void parse();
    bool readStart();
    bool readLine();
    bool readVertex();
    bool readFace();
    bool readLineObj();
    bool readPoint();
    bool readCurve();
    bool readMtlLib();
    bool readObject();
    bool readUseMaterial();
    bool readGroup();

    int pointer;
    std::map<int, std::string> tokens_list;
    std::string name;
    std::vector<Coordinate *> vertex;
    std::list<GraphicObject *> objects;
    std::string file;
};

#endif // OBJECTDESCRIBER_H
	 	  	 	   	      	    		  	     	       	 	
