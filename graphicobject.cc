#include "graphicobject.h"
#include <algorithm>

GraphicObject::GraphicObject(std::string *name, GraphicObjectType type)
{
    this->name = name;
    this->type = type;
    this->strokeColor = Gdk::Color("black");
}

GraphicObject::~GraphicObject()
{

}

std::list<Coordinate*> GraphicObject::getWorldCoordinates()
{

}

GraphicObjectType GraphicObject::getType()
{
    return this->type;
}

std::string GraphicObject::getName()
{
    return *this->name;
}

GraphicObject *GraphicObject::clone()
{

}

Coordinate *GraphicObject::getGeometricCenter()
{
    double x, y, count = 0;
    for (Coordinate *coordinate: this->getWorldCoordinates())
    {	 	  	 	   	      	    		  	     	       	 	
        x += coordinate->getX();
        y += coordinate->getY();
        count++;
    }
    Coordinate *center = new Coordinate(x/count, y/count, 0);
    return center;
}

void GraphicObject::setName(std::string *name)
{
    this->name = name;
}

std::string GraphicObject::toObj()
{
    std::string vertex = "";
    for (Coordinate* coordinate: this->getWorldCoordinates()) {
        vertex = vertex + "v " + std::to_string(coordinate->getX()) + " " +
                 std::to_string(coordinate->getY()) + " " +
                 std::to_string(coordinate->getZ()) + " \n";
    }
    std::string description = "o " + this->getName() + " \n";
    bool curve = false;
    switch (this->getType()) {
    case POINT:
        description = description + "p ";
        break;
    case LINE:
        description = description + "l ";
        break;
    case POLYGON:
        description = description + "f ";
        break;
    case BEZIER:
        description = description + "cstype bezier\ncurv2d ";
        curve = true;
        break;
    case BSPLINE:
        description = description + "cstype bspline\ncurv2d ";
        curve = true;
        break;
    }
    for (int i = -this->getWorldCoordinates().size(); i < 0; i++) {
        description = description + std::to_string(i) + " ";
    }
    description = description + " \n";
    if (curve) {
        description += "end\n";
    }
    std::replace(vertex.begin(), vertex.end(), ',', '.');

    return vertex + description;
}

void GraphicObject::setStrokeColor(Gdk::Color strokeColor)
{
    this->strokeColor = strokeColor;
}

Gdk::Color GraphicObject::getStrokeColor()
{
    return this->strokeColor;
}
