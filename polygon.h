#ifndef POLYGON_H
#define POLYGON_H


#include "graphicobject.h"

class Polygon : public GraphicObject
{
public:
    Polygon(std::string*, std::list<Coordinate*>);
    ~Polygon();
    std::list<Coordinate*> getWorldCoordinates();
    std::list<Coordinate*> getNormalizedCoordinates();
    void setNormalizedCoordinates(std::list<Coordinate*> coordinates);
    Polygon *clone();    
    void setFill(bool);
    bool isFilled();
private:
    std::list<Coordinate*> coordinates;
    std::list<Coordinate*> normalized_coordinates;
    bool fill;
};

#endif // POLYGON_H
