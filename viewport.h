#ifndef VIEWPORT_H
#define VIEWPORT_H


#include <gtkmm/drawingarea.h>

#include "graphicobject.h"
#include "line.h"
#include "point.h"
#include "polygon.h"
#include "bezier.h"
#include "bspline.h"

namespace sgi {

#define OutCode int

const int INSIDE = 0; // 0000
const int LEFT   = 1;   // 0001
const int RIGHT  = 2;  // 0010
const int BOTTOM = 4; // 0100
const int TOP    = 8;    // 1000

class Viewport : public Gtk::DrawingArea
{
public:
    Viewport();
    virtual ~Viewport();
    void redraw(std::list<GraphicObject*>);
    OutCode computeOutCode(double, double);

protected:
    //Override default signal handler:
    bool on_draw(const Cairo::RefPtr<Cairo::Context>&) override;

private:
    void drawObject(const Cairo::RefPtr<Cairo::Context>&, GraphicObject*);
    void drawPoint(const Cairo::RefPtr<Cairo::Context>&, Point*);
    void drawLine(const Cairo::RefPtr<Cairo::Context>&, Line*);
    void drawPolygon(const Cairo::RefPtr<Cairo::Context>&, Polygon*);
    void drawBezierCurve(const Cairo::RefPtr<Cairo::Context>&, Bezier*);
    void drawBSplineCurve(const Cairo::RefPtr<Cairo::Context>&, BSpline*);
    bool insideClipEdge(Coordinate*, OutCode);

    std::list<GraphicObject*> objects_to_draw;

    double xwmax, ywmax, xwmin, ywmin, r_min, r_max, r_max2, xvpmax, yvpmax;
};

}

#endif // VIEWPORT_H
	 	  	 	   	      	    		  	     	       	 	
