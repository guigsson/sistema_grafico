#ifndef DISPLAYFILE_H
#define DISPLAYFILE_H

#include <fstream>
#include <list>
#include <string>

#include "graphicobject.h"
#include "objectdescriber.h"

class DisplayFile
{
public:
    DisplayFile();
    std::list<GraphicObject*> getObjects();
    void addObject(GraphicObject*);
    bool deleteObject(std::string);
    GraphicObject *getObject(std::string);
    void LoadObjects();
    void SaveObjects();
private:
    std::list<GraphicObject*> objects;
    std::string cwd;
    std::string filename = "wavefront.obj";
};

#endif // DISPLAYFILE_H
	 	  	 	   	      	    		  	     	       	 	
