#ifndef POINT_H
#define POINT_H


#include "graphicobject.h"

class Point : public GraphicObject
{
public:
    Point(std::string*, Coordinate*);
    ~Point();
    Coordinate *getCoordinate();
    Coordinate *getNormalizedCoordinate();
    std::list<Coordinate*> getWorldCoordinates();
    std::list<Coordinate*> getNormalizedCoordinates();
    void setNormalizedCoordinates(std::list<Coordinate*> coordinates);
    Point *clone();
private:
    Coordinate *c;
    Coordinate *nc;
};

#endif // POINT_H
