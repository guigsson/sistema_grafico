#include "line.h"

Line::Line(std::string *name, Coordinate *coordinate1, Coordinate *coordinate2) : GraphicObject(name, LINE)
{
    this->c1 = coordinate1;
    this->c2 = coordinate2;

    this->nc1 = 0;
    this->nc2 = 0;
}

Line::~Line()
{

}

std::list<Coordinate*> Line::getWorldCoordinates()
{
    std::list<Coordinate*> coordinates;
    coordinates.push_back(this->c1);
    coordinates.push_back(this->c2);
    return coordinates;
}

std::list<Coordinate*> Line::getNormalizedCoordinates()
{
    std::list<Coordinate*> coordinates;
    coordinates.push_back(this->nc1);
    coordinates.push_back(this->nc2);
    return coordinates;
}

void Line::setNormalizedCoordinates(std::list<Coordinate *> coordinates)
{
    nc1 = coordinates.front();
    nc2 = coordinates.back();
}

Line *Line::clone()
{
    Coordinate *c1Clone = this->c1->clone();
    Coordinate *c2Clone = this->c2->clone();
    Line *line = new Line(this->name, c1Clone, c2Clone);
    line->setNormalizedCoordinates(this->getNormalizedCoordinates());
    return line;
}

Coordinate* Line::computeIntersection(Line* l){
    double x1 = this->c1->getX();
    double y1 = this->c1->getY();
    double x2 = this->c2->getX();
    double y2 = this->c2->getY();

    double x3 = l->c1->getX();
    double y3 = l->c1->getY();
    double x4 = l->c2->getX();
    double y4 = l->c2->getY();

    double d = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
    if (d == 0)
    {
        return (Coordinate*) 0;
    }

    double xi = ((x3-x4)*(x1*y2-y1*x2)-(x1-x2)*(x3*y4-y3*x4))/d;
    double yi = ((y3-y4)*(x1*y2-y1*x2)-(y1-y2)*(x3*y4-y3*x4))/d;

    return new Coordinate(xi,yi, 0);
}
	 	  	 	   	      	    		  	     	       	 	
