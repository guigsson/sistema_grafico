#ifndef BSPLINE_H
#define BSPLINE_H

#include "graphicobject.h"

class BSpline : public GraphicObject
{
public:
    BSpline(std::string*, std::list<Coordinate*>);
    ~BSpline();
    std::list<Coordinate*> getWorldCoordinates();
    std::list<Coordinate*> getNormalizedCoordinates();
    void setNormalizedCoordinates(std::list<Coordinate*> coordinates);
    std::list<Coordinate*> generateCurveCoordinates();
    BSpline *clone();

private:
    std::list<Coordinate*> coordinates;
    std::list<Coordinate*> normalized_coordinates;
};

#endif // BSPLINE_H
