#include "objectdescriber.h"
#include <string.h>
#include <stdlib.h>
#include <algorithm>
#include <regex>
#include <stdexcept>
#include "polygon.h"
#include "point.h"
#include "line.h"
#include "bezier.h"
#include "bspline.h"

ObjectDescriber::ObjectDescriber()
        : pointer(0),
          name(""),
          file("")
{

}

std::string ObjectDescriber::toObj(std::list<GraphicObject *> objects)
{
    std::string obj;
    for (auto object : objects) {
        obj = obj + object->toObj();
    }

    return obj;
}

void ObjectDescriber::tokenize(std::string obj_str)
{
    std::replace( obj_str.begin(), obj_str.end(), '\n', ' ');
    std::istringstream ss(obj_str);
    std::string token;
    int pos = 0;
    char delim = ' ';
    while (std::getline(ss, token, delim)) {	 	  	 	   	      	    		  	     	       	 	
        if (token == "") continue;
        tokens_list[pos] = token;
        std::cout << tokens_list[pos].c_str() << std::endl;
        pos++;
    }

}

std::pair<Token, std::string> ObjectDescriber::getNextToken()
{
    auto token = std::make_pair<Token, std::string>(Token::Invalid, "");
    try {
        std::string token_str = tokens_list.at(pointer);
        token = std::make_pair<Token, std::string>(interpretToken(token_str), std::string(token_str));
        pointer++;
    } catch (...) {
        return std::make_pair<Token, std::string>(Token::Invalid, "");
    }
    return token;
}

std::pair<Token, std::string> ObjectDescriber::peekNextToken(int offset)
{
    auto token = std::make_pair<Token, std::string>(Token::Invalid, "");
    try {
        token = std::make_pair<Token, std::string>(interpretToken(tokens_list.at(pointer + offset)),
                                                   static_cast<std::string>(tokens_list.at(pointer + offset)));
    } catch (...) {
        return std::make_pair<Token, std::string>(Token::Invalid, "");
    }
    return token;
}

Token ObjectDescriber::interpretToken(std::string token)
{
    std::cout << token.c_str() << std::endl;
    if (token == "v") {	 	  	 	   	      	    		  	     	       	 	
        return VerticeDescr;
    } else if (token == "p") {
        return PointObj;
    } else if (token == "cstype") {
        return CurveTypeId;
    } else if (token == "curv2d") {
        return Curve2DObj;
    } else if (token == "bezier") {
        return BezierCurve;
    } else if (token == "bspline") {
        return BSplineCurve;
    } else if (token == "l") {
        return LineObj;
    } else if (token == "f") {
        return PolygonObj;
    } else if (token == "o") {
        return NameTag;
    } else if (token == "end") {
        return EndTag;
    } else if (token == "$") {
        return FileEnd;
    } else {
        try {
           if (std::stoi(token.c_str()) < 0 && token.find(".") == std::string::npos) {
                return NegInteger;
            } else if (std::stoi(token.c_str()) >= 0 && token.find(".") == std::string::npos) {
                return Integer;
            } else if (std::stof(token.c_str()) || std::stof(token.c_str()) + 1) {
                return Double; 
            }
        } catch (...) {
            if (token != "") {
                return ObjName;   
            }
            return Invalid;
        }	 	  	 	   	      	    		  	     	       	 	
    }
}

void ObjectDescriber::parse()
{
    readStart();
}

bool ObjectDescriber::readStart()
{
    bool success = true;
    do {
        success = readLine();
    } while(success != false && peekNextToken(0).first != FileEnd);
    return success;
}

bool ObjectDescriber::readLine()
{
    return (readVertex() ||
            readFace() ||
            readLineObj() ||
            readPoint() ||
            readCurve() ||
            readMtlLib() ||
            readObject() ||
            readUseMaterial() ||
            readGroup());
}

bool ObjectDescriber::readVertex()
{
    if (peekNextToken(0).first != VerticeDescr) {
        return false;
    }
    auto next_token = getNextToken();

    Coordinate *c = new Coordinate(0, 0, 0);
    vertex.push_back(c);
    next_token = getNextToken();
    if (next_token.first != Double) {	 	  	 	   	      	    		  	     	       	 	
        throw std::runtime_error("Parse error: expected double");
    }
    c->setX(std::atof(next_token.second.c_str()));
    next_token = getNextToken();
    if (next_token.first != Double) {
        throw std::runtime_error("Parse error: expected double");
    }
    c->setY(std::atof(next_token.second.c_str()));
    next_token = getNextToken();
    if (next_token.first != Double) {
        throw std::runtime_error("Parse error: expected double");
    }
    c->setZ(std::atof(next_token.second.c_str()));
    return true;
}

bool ObjectDescriber::readFace()
{
    if (peekNextToken(0).first != PolygonObj) {
        return false;
    }
    auto next_token = getNextToken();
    std::list<Coordinate*> coordinates;
    int count = 0;
    while (peekNextToken(0).first == NegInteger || peekNextToken(0).first == Integer) {
        count++;
        next_token = getNextToken();
        int index;
        switch(next_token.first) {
        case NegInteger:
            index = this->vertex.size() + atoi(next_token.second.c_str());
            coordinates.push_back(this->vertex[index]->clone());
            break;
        case Integer:
            index = atoi(next_token.second.c_str()) - 1;
            coordinates.push_back(this->vertex[index]->clone());
            break;
        default:
            return false;
        }	 	  	 	   	      	    		  	     	       	 	
    }
    if (count < 3) {
        throw std::runtime_error("Parse error: expected at least three vertices to create polygon");
    }
    GraphicObject* graphic_object = new Polygon(new std::string(""), coordinates);
    if (name != "") {
        graphic_object->setName(new std::string(name));
        name = "";
    }
    objects.push_back(graphic_object);
    return true;
}

bool ObjectDescriber::readLineObj()
{
    if (peekNextToken(0).first != LineObj) {
        return false;
    }
    auto next_token = getNextToken();
    std::list<Coordinate*> coordinates;
    next_token = getNextToken();
    int index;
    switch(next_token.first) {
    case NegInteger:
        index = this->vertex.size() + atoi(next_token.second.c_str());
        coordinates.push_back(this->vertex[index]->clone());
        break;
    case Integer:
        index = atoi(next_token.second.c_str()) - 1;
        coordinates.push_back(this->vertex[index]->clone());
        break;
    default:
        throw std::runtime_error("Parse error: expected integer");
    }
    next_token = getNextToken();
    switch(next_token.first) {	 	  	 	   	      	    		  	     	       	 	
    case NegInteger:
        index = this->vertex.size()+ atoi(next_token.second.c_str());
        coordinates.push_back(this->vertex[index]->clone());
        break;
    case Integer:
        index =  atoi(next_token.second.c_str()) - 1;
        coordinates.push_back(this->vertex[index]->clone());
        break;
    default:
        throw std::runtime_error("Parse error: expected integer");
    }
    GraphicObject* graphic_object = new Line(new std::string(""), coordinates.front(), coordinates.back());
    if (name != "") {
        graphic_object->setName(new std::string(name));
        name = "";
    }
    objects.push_back(graphic_object);
    return true;
}

bool ObjectDescriber::readPoint()
{
    if (peekNextToken(0).first != PointObj) {
        return false;
    }
    auto next_token = getNextToken();

    std::list<Coordinate*> coordinates;
    next_token = getNextToken();
    int index;
    switch(next_token.first) {
    case NegInteger:
        index = this->vertex.size() + atoi(next_token.second.c_str());
        coordinates.push_back(this->vertex[index]->clone());
        break;
    case Integer:
        index = atoi(next_token.second.c_str()) - 1;
        coordinates.push_back(this->vertex[index]->clone());
        break;
    default:
        throw std::runtime_error("Parse error: expected integer");
    }	 	  	 	   	      	    		  	     	       	 	
    GraphicObject* graphic_object = new Point(new std::string(""), coordinates.front());
    if (name != "") {
        graphic_object->setName(new std::string(name));
        name = "";
    }
    objects.push_back(graphic_object);
    return true;
}

bool ObjectDescriber::readCurve()
{
    if (peekNextToken(0).first != CurveTypeId) {
        return false;
    }
    auto next_token = getNextToken();
    next_token = getNextToken();
    if (!(next_token.first == BezierCurve || next_token.first == BSplineCurve)) {
        throw std::runtime_error("parse error: expected curve type (bezier or bspline)");
    }
    Token curve_type = next_token.first;
    next_token = getNextToken();
    if (next_token.first != Curve2DObj) {
        throw std::runtime_error("parse error: expected 2d curve declaration (curv2d)");
    }
    std::list<Coordinate*> coordinates;
    int count = 0;
    while (peekNextToken(0).first == NegInteger || peekNextToken(0).first == Integer) {
        count++;
        next_token = getNextToken();
        int index;
        switch(next_token.first) {
        case NegInteger:
            index = this->vertex.size() + atoi(next_token.second.c_str());
            coordinates.push_back(this->vertex[index]->clone());
            break;
        case Integer:
            index = atoi(next_token.second.c_str()) - 1;
            coordinates.push_back(this->vertex[index]->clone());
            break;
        case EndTag:
            break;
        default:
            return false;
        }	 	  	 	   	      	    		  	     	       	 	
    }
    GraphicObject* graphic_object;
    switch (curve_type) {
    case BezierCurve:
        if (count != 4) {
            throw std::runtime_error("Parse error: expected exactly 4 vertices to create Bezier curve");
        }
        graphic_object = new Bezier(new std::string(""), coordinates);
        break;
    case BSplineCurve:
        if (count < 4) {
            throw std::runtime_error("Parse error: expected at least 4 vertices to create bspline");
        }
        graphic_object = new BSpline(new std::string(""), coordinates);
        break;
    }
    if (name != "") {
        graphic_object->setName(new std::string(name));
        name = "";
    }
    objects.push_back(graphic_object);
}

bool ObjectDescriber::readMtlLib()
{
    return false;
}

bool ObjectDescriber::readObject()
{
    std::cout << "olar" << std::endl;
    if (peekNextToken(0).first != NameTag) {
        std::cout << "token:" << peekNextToken(0).first << peekNextToken(0).second << std::endl;
        return false;
    }
    auto next_token = getNextToken();
    next_token = getNextToken();
    if(next_token.first != ObjName) {	 	  	 	   	      	    		  	     	       	 	
        throw std::runtime_error("Parse error: expected object name");
    }
    name = next_token.second;
    return true;
}

bool ObjectDescriber::readUseMaterial()
{
    return false;
}

bool ObjectDescriber::readGroup()
{
    return false;
}

std::list<GraphicObject*> ObjectDescriber::fromObj(std::string obj) {
    file = obj;
    if (file.length() == 0) throw new std::runtime_error("obj file empty.");
    tokenize(file);
    tokens_list[tokens_list.size()] = "$";
    std::cout << "checkpoint" << std::endl;
    try {
        parse();
    } catch(std::runtime_error &e) {
        std::cout << e.what() << std::endl;
    }
    return objects;
}
	 	  	 	   	      	    		  	     	       	 	
