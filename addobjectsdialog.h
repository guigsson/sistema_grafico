#ifndef ADDOBJECTSDIALOG_H
#define ADDOBJECTSDIALOG_H


#include <gtkmm.h>

class AddObjectsDialog : public Gtk::Dialog
{

public:
    AddObjectsDialog(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder);
    ~AddObjectsDialog();

protected:
    Glib::RefPtr<Gtk::Builder> builder;

    void addPoint();
    void addLine();
    void addPolygon();
    void addPolygonPoint();
    void deletePolygonPoint();
};

#endif // ADDOBJECTSDIALOG_H
