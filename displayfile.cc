#include "displayfile.h"

#include <sstream>

DisplayFile::DisplayFile()
{
    char path[352];
    getcwd(path, 354);
    cwd = std::string(path);
    filename = cwd + "/" + filename;
    std::cout << filename << std::endl;
}

std::list<GraphicObject*> DisplayFile::getObjects()
{
    return this->objects;
}

void DisplayFile::addObject(GraphicObject *object)
{
    this->objects.push_back(object);
}

bool DisplayFile::deleteObject(std::string name)
{
    for (std::list<GraphicObject*>::iterator it = objects.begin(); it != objects.end(); it++)
    {
        GraphicObject *object = (GraphicObject*) *it;
        if (object->getName().compare(name) == 0)
        {
            objects.remove(object);
            delete object;
            return true;
        }
    }

    return false;
}	 	  	 	   	      	    		  	     	       	 	

GraphicObject* DisplayFile::getObject(std::string name)
{
    for (std::list<GraphicObject*>::iterator it = objects.begin(); it != objects.end(); it++)
    {
        GraphicObject *object = (GraphicObject*) *it;
        if (object->getName().compare(name) == 0)
        {
            return object;
        }
    }
}

void DisplayFile::SaveObjects()
{
    std::ofstream wavefront_file;
    wavefront_file.open(filename);
    if (wavefront_file.is_open()) {
        std::cout << "checkpoint" << std::endl;
        ObjectDescriber describer;
        std::string objects = describer.toObj(getObjects());
        wavefront_file << objects << std::endl;
        std::cout << objects << std::endl;
        wavefront_file.close();
    }
}

void DisplayFile::LoadObjects()
{
    std::ifstream wavefront_file;
    wavefront_file.open(filename);
    if (wavefront_file.is_open()) {
        ObjectDescriber describer;
        std::stringstream stream;
        stream << wavefront_file.rdbuf();
        std::string objects_str = stream.str();
        std::cout << objects_str << std::endl;
        std::list<GraphicObject*> objects;
        try {	 	  	 	   	      	    		  	     	       	 	
            objects = describer.fromObj(objects_str);
        } catch(...) {
        }
        if (objects.size() == 0) {
            wavefront_file.close();
            return;
        }
        for (auto object : objects) {
            addObject(object);
        }
        wavefront_file.close();
    }
//    ObjectDescriber describer;
//    objects.merge(describer.fromObj("v 10.00000 0.00000 0.00000\n"
//                                "v 0.00000 0.00000 0.00000\n"
//                                "o line\n"
//                                "l -2 -1\n"));
}

