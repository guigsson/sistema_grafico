#include "point.h"

Point::Point(std::string *name, Coordinate *coordinate) : GraphicObject(name, POINT)
{
    this->c = coordinate;
    this->nc = 0;
}

Point::~Point()
{

}

Coordinate *Point::getCoordinate()
{
    return this->c;
}

Coordinate *Point::getNormalizedCoordinate()
{
    return this->nc;
}

std::list<Coordinate*> Point::getWorldCoordinates()
{
    std::list<Coordinate*> coordinates;
    coordinates.push_back(this->c);
    return coordinates;
}

std::list<Coordinate*> Point::getNormalizedCoordinates()
{
    std::list<Coordinate*> coordinates;
    coordinates.push_back(this->nc);
    return coordinates;
}

void Point::setNormalizedCoordinates(std::list<Coordinate *> coordinates)
{
    nc = coordinates.front();
}

Point *Point::clone()
{
    Coordinate *coordinateClone = this->c->clone();
    Point *point = new Point(this->name, coordinateClone);
    point->setNormalizedCoordinates(this->getNormalizedCoordinates());
    return point;
}
	 	  	 	   	      	    		  	     	       	 	
