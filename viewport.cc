#include <cairomm/context.h>
#include "viewport.h"
#include "graphicobject.h"

#include <iostream>

using namespace sgi;

Viewport::Viewport()
{
    this->xvpmax = 460.0;
    this->yvpmax = 460.0;

    this->r_min = 10;
    this->r_max = 450;
}

Viewport::~Viewport()
{

}

void Viewport::redraw(std::list<GraphicObject*> objects)
{
    this->objects_to_draw = objects;

    auto win = get_window();
    if (win)
    {
        Gdk::Rectangle r(0, 0, get_allocation().get_width(), get_allocation().get_height());
        win->invalidate_rect(r, false);
    }
}

bool Viewport::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    cr->set_source_rgb(1.0, 1.0, 1.0);
    cr->paint();

    cr->set_source_rgb(0, 0, 0);

    cr->move_to(this->r_min, this->r_min);
    cr->line_to(this->r_max, this->r_min);
    cr->line_to(this->r_max, this->r_max);
    cr->line_to(this->r_min, this->r_max);
    cr->line_to(this->r_min, this->r_min);
    cr->stroke();

    for (GraphicObject *graphic_object: objects_to_draw)
    {	 	  	 	   	      	    		  	     	       	 	
        this->drawObject(cr, graphic_object);
    }

    return true;
}

void Viewport::drawObject(const Cairo::RefPtr<Cairo::Context>& cr, GraphicObject *object)
{
    GraphicObjectType type = object->getType();

    switch(type)
    {
    case POINT:
         this->drawPoint(cr, (Point*)object);
        break;
    case LINE:
        this->drawLine(cr, (Line*)object);
        break;
    case POLYGON:
         this->drawPolygon(cr, (Polygon*)object);
        break;
    case BEZIER:
        this->drawBezierCurve(cr, (Bezier*)object);
        break;
    case BSPLINE:
        this->drawBSplineCurve(cr, (BSpline*)object);
        break;
    }
}

 void Viewport::drawPoint(const Cairo::RefPtr<Cairo::Context>& cr, Point *point)
 {
     Coordinate* coordinate = point->getNormalizedCoordinate();

     double x = coordinate->getX();
     double y = coordinate->getY();

     cr->arc(x, y, 1.0, 0.0, 2*M_PI);
     cr->stroke();
 }

void Viewport::drawLine(const Cairo::RefPtr<Cairo::Context>& cr, Line *line)
{	 	  	 	   	      	    		  	     	       	 	
    std::list<Coordinate*> coordinates = line->getNormalizedCoordinates();

    Coordinate *first_coordinate = coordinates.front();

    double x1 = first_coordinate->getX();
    double y1 = first_coordinate->getY();

    Coordinate *second_coordinate = coordinates.back();

    double x2 = second_coordinate->getX();
    double y2 = second_coordinate->getY();

    //Line Clipping

    OutCode outcode1 = computeOutCode(x1, y1);
    OutCode outcode2 = computeOutCode(x2, y2);

    bool accept = false;

    while (true) {
        if (!(outcode1 | outcode2)) {
            accept = true;
            break;
        } else if (outcode1 & outcode2) {
            break;
        } else {

            double x, y;

            OutCode outcodeOut = outcode1 ? outcode1 : outcode2;

            if (outcodeOut & TOP) {
                x = x1 + (x2 - x1) * (this->r_max - y1) / (y2 - y1);
                y = this->r_max;
            } else if (outcodeOut & BOTTOM) {
                x = x1 + (x2 - x1) * (this->r_min - y1) / (y2 - y1);
                y = this->r_min;
            } else if (outcodeOut & RIGHT) {
                y = y1 + (y2 - y1) * (this->r_max - x1) / (x2 - x1);
                x = this->r_max;
            } else if (outcodeOut & LEFT) {
                y = y1 + (y2 - y1) * (this->r_min - x1) / (x2 - x1);
                x = this->r_min;
            }

            if (outcodeOut == outcode1) {
                x1 = x;
                y1 = y;
                outcode1 = computeOutCode(x1, y1);
            } else {
                x2 = x;
                y2 = y;
                outcode2 = computeOutCode(x2, y2);
            }
        }
    }

    //Line Clipping End
    if (accept) {
        cr->move_to(x1, y1);
        cr->line_to(x2, y2);
        cr->stroke();
    }
}

void Viewport::drawPolygon(const Cairo::RefPtr<Cairo::Context>& cr, Polygon *polygon)
{

    std::list<Coordinate*> vertices = polygon->getNormalizedCoordinates();

    if(polygon->isFilled())
    {
        std::list<Line*> clipPolygon;

        Coordinate* clipPolA = new Coordinate(this->r_min, this->r_min, 0);
        Coordinate* clipPolB = new Coordinate(this->r_min, this->r_max, 0);
        Coordinate* clipPolC = new Coordinate(this->r_max, this->r_max, 0);
        Coordinate* clipPolD = new Coordinate(this->r_max, this->r_min, 0);

        clipPolygon.push_back(new Line(new std::string("foo"), clipPolA, clipPolD));
        clipPolygon.push_back(new Line(new std::string("foo"), clipPolD, clipPolC));
        clipPolygon.push_back(new Line(new std::string("foo"), clipPolC, clipPolB));
        clipPolygon.push_back(new Line(new std::string("foo"), clipPolB, clipPolA));

        OutCode clipEdgesOutcode[4] = {BOTTOM, RIGHT, TOP, LEFT};

        std::list<Coordinate*> output_list;

        std::list<Coordinate*> input_list;
        int count = 0;

        for (Line* clip_edge: clipPolygon)
        {
            OutCode clipEdgeOutcode = clipEdgesOutcode[count];

            input_list = vertices;
            output_list.clear();
            if(!input_list.empty()){
                Coordinate* s = input_list.back();

                for (Coordinate* e: input_list)
                {

                    if (this->insideClipEdge(e, clipEdgeOutcode))
                    {
                        if (!this->insideClipEdge(s, clipEdgeOutcode))
                        {
                            Coordinate* c = clip_edge->computeIntersection(new Line(new std::string("foo"), s, e));
                            if (c != NULL)
                            {
                                output_list.push_back(c);
                            }
                        }
                        output_list.push_back(e);
                    }
                    else if (this->insideClipEdge(s, clipEdgeOutcode))
                    {
                        Coordinate* a = clip_edge->computeIntersection(new Line(new std::string("foo"), s, e));
                        if (a != NULL)
                        {
                            output_list.push_back(a);
                        }
                    }
                    s = e;
                }
            }
            count++;
        }


        Coordinate *first_coordinate = output_list.front();

        cr->move_to(first_coordinate->getX(), first_coordinate->getY());

        for (Coordinate *coordinate: output_list)
        {
            cr->line_to(coordinate->getX(), coordinate->getY());
        }

        cr->line_to(first_coordinate->getX(), first_coordinate->getY());

        cr->stroke();
    }
    else
    {

        int count = 0;
        for (std::list<Coordinate*>::iterator it = vertices.begin(); it != vertices.end(); ++it)
        {
            std::list<Coordinate*>::iterator next_it;

            count++;

            if (count == vertices.size()) {
                next_it = vertices.begin();
            } else {
                next_it = std::next(it, 1);
            }
            Line* line = new Line(new std::string("foo"), *it, *next_it);
            line->setNormalizedCoordinates(line->getWorldCoordinates());
            this->drawLine(cr, line);
        }
    }

}

void Viewport::drawBezierCurve(const Cairo::RefPtr<Cairo::Context>& cr, Bezier *curve)
{

//    std::list<Coordinate*> coordinates = curve->generateCurveCoordinates();
//    for (std::list<Coordinate*>::iterator it = coordinates.begin(); std::next(it, 1) != coordinates.end(); it++)
//    {
//        Line* line = new Line(new std::string("foo"), *it, *std::next(it, 1));
//        line->setNormalizedCoordinates(line->getWorldCoordinates());
//        this->drawLine(cr, line);
//    }


    std::list<Coordinate*> coordinates = curve->getNormalizedCoordinates();
    std::list<Coordinate*>::iterator it = coordinates.begin();

    Coordinate *p1 = *it;
    Coordinate *p2 = *std::next(it, 1);
    Coordinate *p3 = *std::next(it, 2);
    Coordinate *p4 = *std::next(it, 3);

    Matrix<double> *M = new Matrix<double>(
    {
        {-1.0, 3.0, -3.0, 1.0},
        {3.0, -6.0, 3.0, 0.0},
        {-3.0, 3.0, 0.0, 0.0},
        {1.0, 0.0, 0.0, 0.0}
    });

    Matrix<double> *Cx = new Matrix<double>(
    {
        {p1->getX(), p2->getX(), p3->getX(), p4->getX()}
    });

    Matrix<double> *Cy = new Matrix<double>(
    {
        {p1->getY(), p2->getY(), p3->getY(), p4->getY()}
    });

    Matrix<double> BCx = *Cx * *M;
    Matrix<double> BCy = *Cy * *M;

    double passo = 0.01;
    double t1, t2, t3, x, y;
    std::list<Coordinate*> segments;

    for (double j = 0; j <= 1; j += passo)
    {
        t1 = j;
        t2 = t1 * j;
        t3 = t2 * j;

        Matrix<double> *T = new Matrix<double>(
        {
            {t3},
            {t2},
            {t1},
            {1}
        });

        x = (BCx * *T)[0][0];
        y = (BCy * *T)[0][0];

        segments.push_back(new Coordinate(x, y, 0.0));
    }

    for (std::list<Coordinate*>::iterator it = segments.begin(); std::next(it, 1) != segments.end(); it++)
    {
        Line* line = new Line(new std::string("foo"), *it, *std::next(it, 1));
        line->setNormalizedCoordinates(line->getWorldCoordinates());
        this->drawLine(cr, line);
    }
}

void Viewport::drawBSplineCurve(const Cairo::RefPtr<Cairo::Context>& cr, BSpline *curve)
{
    std::list<Coordinate*> coordinates = curve->getNormalizedCoordinates();
    std::list<Coordinate*>::iterator it = coordinates.begin();
    for (int i = 0; i < curve->getNormalizedCoordinates().size() - 3; i++) {

        Coordinate *p1 = *it;
        Coordinate *p2 = *std::next(it, 1);
        Coordinate *p3 = *std::next(it, 2);
        Coordinate *p4 = *std::next(it, 3);
        it = std::next(it, 1);

        Matrix<double> *Mbs = new Matrix<double>(
        {
            {-1.0/6, 1.0/2, -1.0/2, 1.0/6},
            {1.0/2, -1.0, 1.0/2, 0.0},
            {-1.0/2, 0.0, 1.0/2, 0.0},
            {1.0/6, 2.0/3, 1.0/6, 0.0}
        });

        Matrix<double> *Gx = new Matrix<double>(
        {
            {p1->getX()},
            {p2->getX()},
            {p3->getX()},
            {p4->getX()}
        });

        Matrix<double> *Gy = new Matrix<double>(
        {
            {p1->getY()},
            {p2->getY()},
            {p3->getY()},
            {p4->getY()}
        });

        Matrix<double> Cx = *Mbs * *Gx;
        Matrix<double> Cy = *Mbs * *Gy;

        double t = 0.01;

        Matrix<double> *E = new Matrix<double>(
        {
            {0.0, 0.0, 0.0, 1.0},
            {t*t*t, t*t, t, 0.0},
            {6*t*t*t, 2*t*t, 0, 0},
            {6*t*t*t, 0, 0, 0}
        });

        Matrix<double> x = (*E * Cx);
        Matrix<double> y = (*E * Cy);


        double _x, _y, dx, d2x, d3x, dy, d2y, d3y;
        std::list<Coordinate*> segments;

        _x= x[0][0];
        dx= x[1][0];
        d2x= x[2][0];
        d3x= x[3][0];

        _y= y[0][0];
        dy= y[1][0];
        d2y= y[2][0];
        d3y= y[3][0];

        segments.push_back(new Coordinate(_x, _y, 0.0));
        for (double j = 0; j <= 1; j += t)
        {
            _x = _x + dx;
            dx = dx + d2x;
            d2x = d2x + d3x;

            _y = _y + dy;
            dy = dy + d2y;
            d2y = d2y + d3y;

            segments.push_back(new Coordinate(_x, _y, 0.0));
        }

        for (std::list<Coordinate*>::iterator it = segments.begin(); std::next(it, 1) != segments.end(); it++)
        {
            Line* line = new Line(new std::string("foo"), *it, *std::next(it, 1));
            line->setNormalizedCoordinates(line->getWorldCoordinates());
            this->drawLine(cr, line);
        }
    }
}

OutCode Viewport::computeOutCode(double x, double y)
{
    OutCode code = INSIDE;

    if (x < this->r_min)
        code |= LEFT;
    else if (x > this->r_max)
        code |= RIGHT;
    if (y < this->r_min)
        code |= BOTTOM;
    else if (y > this->r_max)
        code |= TOP;

    return code;
}
	 	  	 	   	      	    		  	     	       	 	
bool Viewport::insideClipEdge(Coordinate* p, OutCode clipEdgeOutcode)
{
    double px = p->getX();
    double py = p->getY();

    OutCode outcode = this->computeOutCode(px, py);

    if(outcode == INSIDE)
        return true;
    else{
        if(clipEdgeOutcode == TOP){
            if(outcode == 9 || outcode == 8 || outcode == 10)
                return false;
        }else if(clipEdgeOutcode == BOTTOM){
            if(outcode == 5 || outcode == 4 || outcode == 6)
                return false;
        }else if(clipEdgeOutcode == RIGHT){
            if(outcode == 10 || outcode == 2 || outcode == 6)
                return false;
        } else if(clipEdgeOutcode == LEFT){
            if(outcode == 9 || outcode == 1 || outcode == 5)
                return false;
        }
    }

    return true;
}
