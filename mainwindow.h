#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "displayfile.h"
#include "viewport.h"
#include "window.h"
#include "addobjectsdialog.h"

class MainWindow: public Gtk::Window
{
public:
    MainWindow();
    ~MainWindow();
    void windowShow();
    void openAddObjectsDialog();
    void deleteObject();
    void addPoint();
    void addLine();
    void addPolygon();
    void addPolygonPoint();
    void deletePolygonPoint();
    void addBezier();
    void addBSpline();
    void addBSplinePoint();
    void deleteBSplinePoint();
    void zoomIn();
    void zoomOut();
    void rotateLeft();
    void rotateRight();
    void moveWindowUp();
    void moveWindowDown();
    void moveWindowLeft();
    void moveWindowRight();
    void moveObject();
    void scaleObject();
    void rotateObject();
    void generateNCS();

    Gtk::Window *ui;

private:
    Glib::RefPtr<Gtk::Builder> builder;
    Glib::RefPtr<Gtk::ListStore> objects_name;

    DisplayFile *display_file;
    sgi::Window *window;
    sgi::Viewport *viewport;

    void addObjectName(std::string*, GraphicObjectType);
    void redrawObjects();
};

#endif // MAINWINDOW_H
	 	  	 	   	      	    		  	     	       	 	
