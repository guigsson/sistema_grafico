#ifndef WINDOW_H
#define WINDOW_H

namespace sgi {

enum direction {
    HORIZONTAL, VERTICAL
};

class Window
{
public:
    Window(double, double);
    void move(double, direction);
    void zoom(double);
    void rotate(double);
    double getXMin();
    double getYMin();
    double getXMax();
    double getYMax();
    double getAngle();
    double getWcx();
    double getWcy();
    void setXMin(double);
    void setYMin(double);
    void setXMax(double);
    void setYMax(double);
    void setAngle(double);
    void setWcx(double);
    void setWcy(double);
    void setWidth(double);
    void setHeight(double);
    double getWidth();
    double getHeight();

private:
    double xwmin, ywmin, xwmax, ywmax;
    double angle;
    double wcx, wcy;
    double width;
    double height;
};

}

#endif // WINDOW_H
	 	  	 	   	      	    		  	     	       	 	
