#ifndef COORDINATE_H
#define COORDINATE_H


#include "matrix.h"


class Coordinate
{
public:
    Coordinate(double, double, double);
    ~Coordinate();
    double getX();
    double getY();
    double getZ();
    void setX(double);
    void setY(double);
    void setZ(double);
    Coordinate *clone();
    Matrix<double>* toHomogeneous();
    void fromHomogeneous(Matrix<double>*);
    Coordinate *invert();
    Coordinate &operator*=(const double);
    Coordinate &operator+=(const Coordinate&);
    Coordinate &operator-=(const Coordinate&);
private:
    double x, y, z;
};

inline Coordinate operator+ (Coordinate lhs, const Coordinate& rhs)
{
  lhs += rhs;
  return lhs;
}

inline Coordinate operator- (Coordinate lhs, const Coordinate& rhs)
{
  lhs -= rhs;
  return lhs;
}	 	  	 	   	      	    		  	     	       	 	

inline Coordinate operator* (Coordinate lhs, const double& factor)
{
  lhs *= factor;
  return lhs;
}

#endif // COORDINATE_H
