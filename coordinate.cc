#include "coordinate.h"

Coordinate::Coordinate(double x, double y, double z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

Coordinate::~Coordinate()
{

}

double Coordinate::getX()
{
    return this->x;
}

void Coordinate::setX(double x)
{
    this->x = x;
}

double Coordinate::getY()
{
    return this->y;
}

void Coordinate::setY(double y)
{
    this->y = y;
}

double Coordinate::getZ()
{
    return this->z;
}	 	  	 	   	      	    		  	     	       	 	

void Coordinate::setZ(double z)
{
    this->z = z;
}

Coordinate *Coordinate::clone()
{
    return new Coordinate(x, y, z);
}

Matrix<double>* Coordinate::toHomogeneous()
{
    Matrix<double> *homogeneous = new Matrix<double>(
    {
        {this->x, this->y, 1.0}
    });

    return homogeneous;
}

void Coordinate::fromHomogeneous(Matrix<double> *homogeneous)
{
    std::vector<std::vector<double>> elem = homogeneous->get_elements();
    std::vector<double> row = elem.at(0);

    this->x = row.at(0);
    this->y = row.at(1);
}

Coordinate* Coordinate::invert()
{
    return new Coordinate(-this->x, -this->y, -this->z);
}

Coordinate &Coordinate::operator*=(const double factor)
{
    this->x *= factor;
    this->y *= factor;
    this->z *= factor;
    return *this;
}	 	  	 	   	      	    		  	     	       	 	

Coordinate &Coordinate::operator+=(const Coordinate &other)
{
    this->x += other.x;
    this->y += other.y;
    this->z += other.z;
    return *this;
}

Coordinate &Coordinate::operator-=(const Coordinate &other)
{
    this->x -= other.x;
    this->y -= other.y;
    this->z -= other.z;
    return *this;
}

