#include "window.h"

using namespace sgi;

Window::Window(double width, double height)
{
    this->setWidth(width);
    this->setHeight(height);
    this->angle = 0;
}

void Window::setWidth(double width)
{
    this->width = width;
    this->xwmin = -width / 2;
    this->xwmax = width / 2;
    this->wcx = (this->xwmin + this->xwmax) / 2;
}

void Window::setHeight(double height)
{
    this->height = height;
    this->ywmin = -height / 2;
    this->ywmax = height / 2;
    this->wcy = (this->ywmin + this->ywmax) / 2;
}

double Window::getWidth()
{
    return this->width;
}

double Window::getHeight()
{
    return this->height;
}

double Window::getXMax()
{
    return this->xwmax;
}	 	  	 	   	      	    		  	     	       	 	

double Window::getXMin()
{
    return this->xwmin;
}

double Window::getYMax()
{
    return this->ywmax;
}

double Window::getAngle()
{
    return this->angle;
}

double Window::getWcx()
{
    return this->wcx;
}

double Window::getWcy()
{
    return this->wcy;
}

double Window::getYMin()
{
    return this->ywmin;
}

void Window::setXMin(double xwmin)
{
    this->xwmin = xwmin;
}

void Window::setYMin(double ywmin)
{
    this->ywmin = ywmin;
}

void Window::setXMax(double xwmax)
{
    this->xwmax = xwmax;
}

void Window::setYMax(double ywmax)
{
    this->ywmax = ywmax;
}

void Window::setAngle(double angle)
{
    this->angle = angle;
}

void Window::setWcx(double wcx)
{
    this->wcx = wcx;
}

void Window::setWcy(double wcy)
{
    this->wcy = wcy;
}

void Window::move(double variation, direction direction)
{	 	  	 	   	      	    		  	     	       	 	
    switch (direction) {
    case VERTICAL:
        setYMax(getYMax() + variation);
        setYMin(getYMin() + variation);
        setWcy(getWcy() + variation);
        break;
    case HORIZONTAL:
        setXMax(getXMax() + variation);
        setXMin(getXMin() + variation);
        setWcx(getWcx() + variation);
        break;
    }
}

void Window::zoom(double rate)
{
    rate /= 100;
    setWidth(getWidth() * (1 - rate));
    setHeight(getHeight() * (1 - rate));
}

void Window::rotate(double angle)
{
    this->angle = this->angle + angle;
}
