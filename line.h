#ifndef LINE_H
#define LINE_H


#include "graphicobject.h"

class Line : public GraphicObject
{
public:
    Line(std::string*, Coordinate*, Coordinate*);
    ~Line();
    std::list<Coordinate*> getWorldCoordinates();
    std::list<Coordinate*> getNormalizedCoordinates();
    void setNormalizedCoordinates(std::list<Coordinate*> coordinates);
    Line *clone();
    Coordinate* computeIntersection(Line*);
private:
    Coordinate *c1;
    Coordinate *c2;

    Coordinate *nc1;
    Coordinate *nc2;
};

#endif // LINE_H
	 	  	 	   	      	    		  	     	       	 	
