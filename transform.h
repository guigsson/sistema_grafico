#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "matrix.h"
#include "graphicobject.h"

#include <math.h>

enum rotate_direction {
    ROTATE_LEFT, ROTATE_RIGHT
        };

enum rotationType
{
    ORIGIN   = 0,
    CENTER   = 1,
};

enum axis
{
    X_AXIS,
    Y_AXIS,
    Z_AXIS,
};

class Transform
{
private:
    Transform();

public:
    static Transform* getInstance();
    void transform(std::list<GraphicObject*>&, double, double, double, double, double, double, double, double);
    Matrix<double>* getTranslationMatrix(Coordinate*);
    Matrix<double>* getScaleMatrix(Coordinate*);
    Matrix<double>* getRotationMatrix(double);
    void translateObject(GraphicObject*, Coordinate*);
    void scaleObjectOrigin(GraphicObject*, Coordinate*);
    void scaleObjectGeometricCenter(GraphicObject*, Coordinate*);
    void rotateObject(GraphicObject*, Coordinate*, double);
    void transformObject(GraphicObject*, Matrix<double>*);
};

#endif // TRANSFORM_H
	 	  	 	   	      	    		  	     	       	 	
