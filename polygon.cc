#include "polygon.h"

Polygon::Polygon(std::string *name, std::list<Coordinate*> coordinates) : GraphicObject(name, POLYGON)
{
    this->coordinates = coordinates;
    this->fill = false;
}

Polygon::~Polygon()
{

}

std::list<Coordinate*> Polygon::getWorldCoordinates()
{
    return this->coordinates;
}

std::list<Coordinate *> Polygon::getNormalizedCoordinates()
{
    return this->normalized_coordinates;
}

void Polygon::setNormalizedCoordinates(std::list<Coordinate *> coordinates)
{
    this->normalized_coordinates = coordinates;
}

Polygon *Polygon::clone()
{
    std::list<Coordinate*> coordClone;

    for (Coordinate *coordinate: this->coordinates) {
        coordClone.push_back(coordinate->clone());
    }
    Polygon *polygon = new Polygon(this->name, coordClone);
    polygon->setNormalizedCoordinates(this->getNormalizedCoordinates());
    polygon->setStrokeColor(this->getStrokeColor());
    polygon->setFill(this->isFilled());
    return polygon;
}

bool Polygon::isFilled()
{
    return fill;
}

void Polygon::setFill(bool flag)
{
    this->fill = flag;
}

	 	  	 	   	      	    		  	     	       	 	
