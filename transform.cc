#include "transform.h"

static Transform *instance = 0;

Transform *Transform::getInstance()
{
    if(instance == 0)
        instance = new Transform();
    return instance;
}

Transform::Transform()
{
}

void Transform::transform(std::list<GraphicObject*> &objects, double xwmin, double ywmin, double xwmax, double ywmax,
                          double xvpmin, double yvpmin, double xvpmax, double yvpmax)
{
    for(GraphicObject *object: objects) {
        for(Coordinate *coordinate: object->getNormalizedCoordinates()) {
            coordinate->setX(((coordinate->getX() + 1)/2) * (xvpmax - xvpmin));
            coordinate->setY((1 - ((coordinate->getY() + 1) / 2)) * (yvpmax - yvpmin));
            std::cout << "x: " << coordinate->getX() << std::endl;
            std::cout << "y: " << coordinate->getY() << std::endl;

        }
    }
}

Matrix<double>* Transform::getTranslationMatrix(Coordinate *coordinate)
{
    Matrix<double> *m = new Matrix<double>(
    {
        {1.0, 0.0, 0.0},
        {0.0, 1.0, 0.0},
        {coordinate->getX(), coordinate->getY(), 1.0}
    });

    return m;
}	 	  	 	   	      	    		  	     	       	 	

Matrix<double>* Transform::getScaleMatrix(Coordinate *coordinate)
{
    Matrix<double> *m = new Matrix<double>(
    {
        {coordinate->getX(), 0.0, 0.0},
        {0.0, coordinate->getY(), 0.0},
        {0.0, 0.0, 1.0}
    });

    return m;
}

Matrix<double>* Transform::getRotationMatrix(double angle)
{
    Matrix<double> *m = new Matrix<double>(
    {
        {cos(angle), -sin(angle), 0.0},
        {sin(angle), cos(angle), 0.0},
        {0.0, 0.0, 1.0}
    });
    return m;
}

void Transform::translateObject(GraphicObject *object, Coordinate *coordinate)
{
    Matrix<double> *m = getTranslationMatrix(coordinate);
    this->transformObject(object, m);
}

void Transform::scaleObjectOrigin(GraphicObject *object, Coordinate *factor)
{
    Matrix<double> *m = getScaleMatrix(factor);
    this->transformObject(object, m);
}

void Transform::scaleObjectGeometricCenter(GraphicObject *object, Coordinate *factor)
{
    Coordinate *geometricCenter = object->getGeometricCenter();
    Matrix<double> *translationFromOrigin = getTranslationMatrix(geometricCenter);
    *geometricCenter *= -1.0;
    Matrix<double> *translationToOrigin = getTranslationMatrix(geometricCenter);
    Matrix<double> *scale = getScaleMatrix(new Coordinate(factor->getX(), factor->getY(), 0));

    Matrix<double> m = *translationToOrigin * *scale;
    m = m * *translationFromOrigin;

    this->transformObject(object, &m);
}	 	  	 	   	      	    		  	     	       	 	

void Transform::rotateObject(GraphicObject *object, Coordinate *point, double angle)
{
    double radian = angle * M_PI / 180.0;
    Coordinate *origin = new Coordinate(0.0, 0.0, 0.0);
    Coordinate dif = *origin - *point;
    Matrix<double> *translationToPoint = getTranslationMatrix(&dif);
    Matrix<double> *rotation = getRotationMatrix(radian);
    dif *= -1.0;
    Matrix<double> *translationFromPoint = getTranslationMatrix(&dif);

    Matrix<double> m =  *translationToPoint * *rotation * *translationFromPoint;

    this->transformObject(object, &m);
}

void Transform::transformObject(GraphicObject *object, Matrix<double> *m)
{
    for (Coordinate *coordinate: object->getWorldCoordinates())
    {
        Matrix<double> result = *coordinate->toHomogeneous() * *m;
        coordinate->fromHomogeneous(&result);
    }
}

	 	  	 	   	      	    		  	     	       	 	
