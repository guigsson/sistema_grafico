#include "mainwindow.h"

#include "line.h"
#include "point.h"
#include "polygon.h"
#include "transform.h"

#include <iostream>

class ObjectNameColumn: public Gtk::TreeModel::ColumnRecord {
    public:
        ObjectNameColumn() {
            // This order must match the column order in the .glade file
            this->add(this->object_name_column);
        }

        // These types must match those for the model in the .glade file
        Gtk::TreeModelColumn<Glib::ustring> object_name_column;
};

class PolygonPointsColumn: public Gtk::TreeModel::ColumnRecord {
    public:
        PolygonPointsColumn() {
            // This order must match the column order in the .glade file
            this->add(this->x_column);
            this->add(this->y_column);
            this->add(this->z_column);
        }

        // These types must match those for the model in the .glade file
        Gtk::TreeModelColumn<double> x_column;
        Gtk::TreeModelColumn<double> y_column;
        Gtk::TreeModelColumn<double> z_column;
};

class BSplinePointsColumn: public Gtk::TreeModel::ColumnRecord {
    public:
        BSplinePointsColumn() {	 	  	 	   	      	    		  	     	       	 	
            // This order must match the column order in the .glade file
            this->add(this->x_column);
            this->add(this->y_column);
            this->add(this->z_column);
        }

        // These types must match those for the model in the .glade file
        Gtk::TreeModelColumn<double> x_column;
        Gtk::TreeModelColumn<double> y_column;
        Gtk::TreeModelColumn<double> z_column;
};

MainWindow::MainWindow()
{
    //Load the Glade file and instiate its widgets:
    try
    {
        builder = Gtk::Builder::create_from_file("mainwindow.glade");
    }
    catch (const Glib::FileError & ex)
    {
        std::cerr << ex.what() << std::endl;
    }
    catch(const Glib::MarkupError& ex)
    {
        std::cerr << "MarkupError: " << ex.what() << std::endl;
    }
    catch(const Gtk::BuilderError& ex)
    {
        std::cerr << "BuilderError: " << ex.what() << std::endl;
    }

    builder->get_widget("main_window", ui);

    this->display_file = new DisplayFile();
    this->window = new sgi::Window(500.0, 500.0);

    this->viewport = new sgi::Viewport();

    Glib::RefPtr<Gtk::Frame> viewport_container = Glib::RefPtr<Gtk::Frame>::cast_dynamic(
        builder->get_object("viewport_container")
    );
    viewport_container->add(*viewport);
    viewport->show();

    Gtk::Window* main_window;
    builder->get_widget("main_window", main_window);
    main_window->signal_show().connect(sigc::mem_fun(this, &MainWindow::windowShow));

    // Objects
    Gtk::Button* button_add_object = nullptr;
    builder->get_widget("button_add_object", button_add_object);
    button_add_object->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::openAddObjectsDialog));

    Gtk::Button* button_delete_object = nullptr;
    builder->get_widget("button_delete_object", button_delete_object);
    button_delete_object->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::deleteObject));

    Gtk::TreeView* tv = nullptr;
    builder->get_widget("objects_tree_view", tv); // refBuilder is Gtk::Builder instance

    objects_name = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(
        builder->get_object("objects_list_store")
    );

    tv->set_model(objects_name);

    // Window movement
    Gtk::Button* button_window_up = nullptr;
    builder->get_widget("button_window_up", button_window_up);
    button_window_up->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::moveWindowUp));

    Gtk::Button* button_window_right = nullptr;
    builder->get_widget("button_window_right", button_window_right);
    button_window_right->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::moveWindowRight));

    Gtk::Button* button_window_down = nullptr;
    builder->get_widget("button_window_down", button_window_down);
    button_window_down->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::moveWindowDown));

    Gtk::Button* button_window_left = nullptr;
    builder->get_widget("button_window_left", button_window_left);
    button_window_left->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::moveWindowLeft));

    // Window zoom
    Gtk::Button* button_zoom_in = nullptr;
    builder->get_widget("button_zoom_in", button_zoom_in);
    button_zoom_in->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::zoomIn));

    Gtk::Button* button_zoom_out = nullptr;
    builder->get_widget("button_zoom_out", button_zoom_out);
    button_zoom_out->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::zoomOut));

    // Window rotation
    Gtk::Button* button_rotate_left = nullptr;
    builder->get_widget("button_rotate_left", button_rotate_left);
    button_rotate_left->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::rotateLeft));

    Gtk::Button* button_rotate_right = nullptr;
    builder->get_widget("button_rotate_right", button_rotate_right);
    button_rotate_right->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::rotateRight));

    // Add dialog
    Gtk::Button* button_add_point = nullptr;
    builder->get_widget("button_add_point", button_add_point);
    button_add_point->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::addPoint));

    Gtk::Button* button_add_line = nullptr;
    builder->get_widget("button_add_line", button_add_line);
    button_add_line->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::addLine));

    Gtk::Button* button_add_polygon = nullptr;
    builder->get_widget("button_add_polygon", button_add_polygon);
    button_add_polygon->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::addPolygon));

    Gtk::Button* button_add_polygon_point = nullptr;
    builder->get_widget("button_add_polygon_point", button_add_polygon_point);
    button_add_polygon_point->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::addPolygonPoint));

    Gtk::Button* button_delete_polygon_point = nullptr;
    builder->get_widget("button_delete_polygon_point", button_delete_polygon_point);
    button_delete_polygon_point->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::deletePolygonPoint));

    Gtk::Button* button_add_bspline = nullptr;
    builder->get_widget("button_add_bspline", button_add_bspline);
    button_add_bspline->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::addBSpline));

    Gtk::Button* button_add_bspline_point = nullptr;
    builder->get_widget("button_add_bspline_point", button_add_bspline_point);
    button_add_bspline_point->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::addBSplinePoint));

    Gtk::Button* button_delete_bspline_point = nullptr;
    builder->get_widget("button_delete_bspline_point", button_delete_bspline_point);
    button_delete_bspline_point->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::deleteBSplinePoint));

    Gtk::Button* button_add_curve = nullptr;
    builder->get_widget("button_add_curve", button_add_curve);
    button_add_curve->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::addBezier));


    // Objects Transformations
    Gtk::Button* button_translate_ok = nullptr;
    builder->get_widget("button_translate_ok", button_translate_ok);
    button_translate_ok->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::moveObject));

    Gtk::Button* button_scale_ok = nullptr;
    builder->get_widget("button_scale_ok", button_scale_ok);
    button_scale_ok->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::scaleObject));

    Gtk::Button* button_rotate_ok = nullptr;
    builder->get_widget("button_rotate_ok", button_rotate_ok);
    button_rotate_ok->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::rotateObject));


    show_all_children();
}	 	  	 	   	      	    		  	     	       	 	

MainWindow::~MainWindow()
{

}

void MainWindow::windowShow()
{
    this->display_file->LoadObjects();
    for (auto object : display_file->getObjects()) {
        this->addObjectName(new std::string(object->getName().c_str()), object->getType());
    }
    this->redrawObjects();
}

void MainWindow::openAddObjectsDialog()
{
    AddObjectsDialog *add_object_dialog;
    builder->get_widget_derived("add_object_dialog", add_object_dialog);
    add_object_dialog->show();
}


void MainWindow::deleteObject()
{
    Gtk::TreeView* objects_tree = nullptr;
    builder->get_widget("objects_tree_view", objects_tree); // refBuilder is Gtk::Builder instance

    Glib::RefPtr<Gtk::TreeSelection> selection = objects_tree->get_selection();
    Gtk::TreeModel::iterator selected_row = selection->get_selected();
    Gtk::TreeModel::Row row = *selected_row;
    ObjectNameColumn columns;

    std::string *name = new std::string(row.get_value(columns.object_name_column));

    if (this->display_file->deleteObject(*name))
    {	 	  	 	   	      	    		  	     	       	 	
        objects_name->erase(selected_row);
        this->redrawObjects();
    }

}

void MainWindow::addPoint()
{

    Gtk::Entry *point_name;
    this->builder->get_widget("point_name", point_name);

    std::string *name = new std::string(point_name->get_text());

    if(name->compare("") == 0)
    {
        Gtk::MessageDialog dlg("Missing required field 'Name'.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        dlg.set_title("Error");
        dlg.run();
    }
    else
    {
        Gtk::SpinButton *point_x;
        this->builder->get_widget("point_x", point_x);
        Gtk::SpinButton *point_y;
        this->builder->get_widget("point_y", point_y);
        Gtk::SpinButton *point_z;
        this->builder->get_widget("point_z", point_z);

        Coordinate *coordinate = new Coordinate(point_x->get_value(), point_y->get_value(), point_z->get_value());
        Point *point = new Point(name, coordinate);
        this->display_file->addObject(point);
        this->addObjectName(name, POINT);
        this->redrawObjects();
    }
}	 	  	 	   	      	    		  	     	       	 	

void MainWindow::addLine()
{
    Gtk::Entry *line_name;
    this->builder->get_widget("line_name", line_name);

    std::string *name = new std::string(line_name->get_text());

    if(name->compare("") == 0)
    {
        Gtk::MessageDialog dlg("Missing required field 'Name'.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        dlg.set_title("Error");
        dlg.run();
    }
    else
    {
        Gtk::SpinButton *line_x1;
        this->builder->get_widget("line_x1", line_x1);
        Gtk::SpinButton *line_y1;
        this->builder->get_widget("line_y1", line_y1);
        Gtk::SpinButton *line_z1;
        this->builder->get_widget("line_z1", line_z1);

        Gtk::SpinButton *line_x2;
        this->builder->get_widget("line_x2", line_x2);
        Gtk::SpinButton *line_y2;
        this->builder->get_widget("line_y2", line_y2);
        Gtk::SpinButton *line_z2;
        this->builder->get_widget("line_z2", line_z2);

        Coordinate *coordinate1 = new Coordinate(line_x1->get_value(), line_y1->get_value(), line_z1->get_value());
        Coordinate *coordinate2 = new Coordinate(line_x2->get_value(), line_y2->get_value(), line_z2->get_value());
        Line *line = new Line(name, coordinate1, coordinate2);
        this->display_file->addObject(line);
        this->addObjectName(name, LINE);
        this->redrawObjects();
    }	 	  	 	   	      	    		  	     	       	 	
}


void MainWindow::addPolygon()
{
    Gtk::Entry *polygon_name;
    this->builder->get_widget("polygon_name", polygon_name);

    std::string *name = new std::string(polygon_name->get_text());

    if(name->compare("") == 0)
    {
        Gtk::MessageDialog dlg("Missing required field 'Name'.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        dlg.set_title("Error");
        dlg.run();
    }
    else
    {
        std::list<Coordinate*> coordinates;
        PolygonPointsColumn columns;

        Glib::RefPtr<Gtk::ListStore> polygon_points_store;

        polygon_points_store = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(
            builder->get_object("polygon_points_store")
        );

        typedef Gtk::TreeModel::Children type_children;
        type_children children = polygon_points_store->children();

        if (children.size() < 3)
        {
            Gtk::MessageDialog dlg("Required at least 3 points.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
            dlg.set_title("Error");
            dlg.run();
        } else
        {	 	  	 	   	      	    		  	     	       	 	
            for(type_children::iterator iter = children.begin();
                iter != children.end(); ++iter)
            {
                Gtk::TreeModel::Row row = *iter;
                //Do something with the row - see above for set/get.
                double x = row[columns.x_column];
                double y = row[columns.y_column];
                double z = row[columns.z_column];

                Coordinate *coordinate = new Coordinate(x, y, z);
                coordinates.push_back(coordinate);
            }

            Polygon *polygon = new Polygon(name, coordinates);
            this->display_file->addObject(polygon);
            this->addObjectName(name, POLYGON);
            this->redrawObjects();
        }
    }
}

void MainWindow::addPolygonPoint()
{
    Gtk::SpinButton *polygon_x;
    this->builder->get_widget("polygon_x", polygon_x);
    Gtk::SpinButton *polygon_y;
    this->builder->get_widget("polygon_y", polygon_y);
    Gtk::SpinButton *polygon_z;
    this->builder->get_widget("polygon_z", polygon_z);

    PolygonPointsColumn columns;

    Glib::RefPtr<Gtk::ListStore> polygon_points_store;

    polygon_points_store = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(
        builder->get_object("polygon_points_store")
    );

    auto row = *(polygon_points_store->append());
    row[columns.x_column] = polygon_x->get_value();
    row[columns.y_column] = polygon_y->get_value();
    row[columns.z_column] = polygon_z->get_value();
}	 	  	 	   	      	    		  	     	       	 	

void MainWindow::deletePolygonPoint()
{
    Gtk::TreeView* polygon_points = nullptr;
    builder->get_widget("polygon_points", polygon_points);

    Glib::RefPtr<Gtk::TreeSelection> selection = polygon_points->get_selection();

    Glib::RefPtr<Gtk::ListStore> polygon_points_store;

    polygon_points_store = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(
        builder->get_object("polygon_points_store")
    );

    std::vector<Gtk::TreeModel::Path> paths = selection->get_selected_rows();
    for(int i = paths.size()-1; i>=0; i--)
    {
        polygon_points_store->erase(polygon_points_store->get_iter(paths[i]));
    }
}

void MainWindow::addBezier()
{

    Gtk::Entry *curve_name;
    this->builder->get_widget("curve_name", curve_name);

    std::string *name = new std::string(curve_name->get_text());

    if(name->compare("") == 0)
    {
        Gtk::MessageDialog dlg("Missing required field 'Name'.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        dlg.set_title("Error");
        dlg.run();
    }
    else
    {	 	  	 	   	      	    		  	     	       	 	
        std::list<Coordinate*> coordinates;

        Gtk::SpinButton *p1_x;
        this->builder->get_widget("p1_x", p1_x);
        Gtk::SpinButton *p1_y;
        this->builder->get_widget("p1_y", p1_y);

        Coordinate *p1 = new Coordinate(p1_x->get_value(), p1_y->get_value(), 0.0);
        coordinates.push_back(p1);

        Gtk::SpinButton *p2_x;
        this->builder->get_widget("p2_x", p2_x);
        Gtk::SpinButton *p2_y;
        this->builder->get_widget("p2_y", p2_y);

        Coordinate *p2 = new Coordinate(p2_x->get_value(), p2_y->get_value(), 0.0);
        coordinates.push_back(p2);

        Gtk::SpinButton *p3_x;
        this->builder->get_widget("p3_x", p3_x);
        Gtk::SpinButton *p3_y;
        this->builder->get_widget("p3_y", p3_y);

        Coordinate *p3 = new Coordinate(p3_x->get_value(), p3_y->get_value(), 0.0);
        coordinates.push_back(p3);

        Gtk::SpinButton *p4_x;
        this->builder->get_widget("p4_x", p4_x);
        Gtk::SpinButton *p4_y;
        this->builder->get_widget("p4_y", p4_y);

        Coordinate *p4 = new Coordinate(p4_x->get_value(), p4_y->get_value(), 0.0);
        coordinates.push_back(p4);

        Bezier *bezier = new Bezier(name, coordinates);
        this->display_file->addObject(bezier);
        this->addObjectName(name, BEZIER);

//            BSpline *bspline = new BSpline(name, coordinates);
//            this->display_file->addObject(bspline);
//            this->addObjectName(name, BSPLINE);

        this->redrawObjects();
    }	 	  	 	   	      	    		  	     	       	 	
}

void MainWindow::addBSpline()
{
    Gtk::Entry *bspline_name;
    this->builder->get_widget("bspline_name", bspline_name);

    std::string *name = new std::string(bspline_name->get_text());

    if(name->compare("") == 0)
    {
        Gtk::MessageDialog dlg("Missing required field 'Name'.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        dlg.set_title("Error");
        dlg.run();
    }
    else
    {
        std::list<Coordinate*> coordinates;
        BSplinePointsColumn columns;

        Glib::RefPtr<Gtk::ListStore> bspline_points_store;

        bspline_points_store = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(
            builder->get_object("bspline_points_store")
        );

        typedef Gtk::TreeModel::Children type_children;
        type_children children = bspline_points_store->children();

        if (children.size() < 4)
        {
            Gtk::MessageDialog dlg("Required at least 4 points.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
            dlg.set_title("Error");
            dlg.run();
        } else
        {	 	  	 	   	      	    		  	     	       	 	
            for(type_children::iterator iter = children.begin();
                iter != children.end(); ++iter)
            {
                Gtk::TreeModel::Row row = *iter;
                //Do something with the row - see above for set/get.
                double x = row[columns.x_column];
                double y = row[columns.y_column];
                double z = row[columns.z_column];

                Coordinate *coordinate = new Coordinate(x, y, z);
                coordinates.push_back(coordinate);
            }

            BSpline *bspline = new BSpline(name, coordinates);
            this->display_file->addObject(bspline);
            this->addObjectName(name, BSPLINE);
            this->redrawObjects();
        }
    }
}

void MainWindow::addBSplinePoint()
{
    Gtk::SpinButton *bspline_x;
    this->builder->get_widget("bspline_x", bspline_x);
    Gtk::SpinButton *bspline_y;
    this->builder->get_widget("bspline_y", bspline_y);
    Gtk::SpinButton *bspline_z;
    this->builder->get_widget("bspline_z", bspline_z);

    BSplinePointsColumn columns;

    Glib::RefPtr<Gtk::ListStore> bspline_points_store;

    bspline_points_store = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(
        builder->get_object("bspline_points_store")
    );

    auto row = *(bspline_points_store->append());
    row[columns.x_column] = bspline_x->get_value();
    row[columns.y_column] = bspline_y->get_value();
    row[columns.z_column] = bspline_z->get_value();

}	 	  	 	   	      	    		  	     	       	 	

void MainWindow::deleteBSplinePoint()
{
    Gtk::TreeView* bspline_points = nullptr;
    builder->get_widget("bspline_points", bspline_points);

    Glib::RefPtr<Gtk::TreeSelection> selection = bspline_points->get_selection();

    Glib::RefPtr<Gtk::ListStore> bspline_points_store;

    bspline_points_store = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(
        builder->get_object("bspline_points_store")
    );

    std::vector<Gtk::TreeModel::Path> paths = selection->get_selected_rows();
    for(int i = paths.size()-1; i>=0; i--)
    {
        bspline_points_store->erase(bspline_points_store->get_iter(paths[i]));
    }
}

void MainWindow::addObjectName(std::string *name, GraphicObjectType type)
{
    std::string type_name;

    if (type == POINT) {
        type_name = "Point";
    } else if (type == LINE) {
        type_name = "Line";
    } else if (type == POLYGON) {
        type_name = "Polygon";
    } else if (type == BEZIER) {
        type_name = "Bezier";
    } else if (type == BSPLINE) {
        type_name == "BSpline";
    }	 	  	 	   	      	    		  	     	       	 	

    ObjectNameColumn columns;

    auto row = *(objects_name->append());
    row[columns.object_name_column] = name->data();
}

void MainWindow::redrawObjects()
{
    std::list<GraphicObject*> to_transform = this->display_file->getObjects();
    std::list<GraphicObject*> transfomed_objects;
    generateNCS();
    for(GraphicObject *graphic_object: to_transform) {
        GraphicObject *clone = graphic_object->clone();
        transfomed_objects.push_back(clone);
    }

    Transform::getInstance()->transform(transfomed_objects, window->getXMin(), window->getYMin(), window->getXMax(), window->getYMax(),
                         10, 10, 450, 450);

    display_file->SaveObjects();
    viewport->redraw(transfomed_objects);
}

void MainWindow::generateNCS()
{
    std::list<GraphicObject*> objects = this->display_file->getObjects();
    Coordinate* window_center = new Coordinate(window->getWcx(), window->getWcy(), 0);
    Coordinate* scale = new Coordinate(1 / window->getWidth(), 1 / window->getHeight(), 1);
    Coordinate* origin = new Coordinate(0, 0, 0);
    double angle = window->getAngle();

    for (auto object : objects) {
        GraphicObject* normalized = object->clone();
        Transform::getInstance()->translateObject(normalized, window_center->invert());
        Transform::getInstance()->rotateObject(normalized, origin, angle);
        Transform::getInstance()->scaleObjectOrigin(normalized, scale);
        object->setNormalizedCoordinates(normalized->getWorldCoordinates());
        delete normalized;
    }	 	  	 	   	      	    		  	     	       	 	
}

void MainWindow::zoomIn()
{
    Gtk::SpinButton *zoom_rate;
    this->builder->get_widget("zoom_rate", zoom_rate);

    this->window->zoom(zoom_rate->get_value());
    this->redrawObjects();
}

void MainWindow::zoomOut()
{
    Gtk::SpinButton *zoom_rate;
    this->builder->get_widget("zoom_rate", zoom_rate);

    this->window->zoom(-zoom_rate->get_value());
    this->redrawObjects();
}

void MainWindow::rotateLeft()
{
    Gtk::SpinButton *rotate_window_angle;
    this->builder->get_widget("rotate_window_angle", rotate_window_angle);

    this->window->rotate(rotate_window_angle->get_value());
    this->redrawObjects();
}

void MainWindow::rotateRight()
{
    Gtk::SpinButton *rotate_window_angle;
    this->builder->get_widget("rotate_window_angle", rotate_window_angle);

    this->window->rotate(-rotate_window_angle->get_value());
    this->redrawObjects();
}	 	  	 	   	      	    		  	     	       	 	

void MainWindow::moveWindowUp()
{
    Gtk::SpinButton *translate_rate;
    this->builder->get_widget("translate_rate", translate_rate);

    this->window->move(translate_rate->get_value(), sgi::VERTICAL);
    this->redrawObjects();
}

void MainWindow::moveWindowDown()
{
    Gtk::SpinButton *translate_rate;
    this->builder->get_widget("translate_rate", translate_rate);

    this->window->move(-translate_rate->get_value(), sgi::VERTICAL);
    this->redrawObjects();
}

void MainWindow::moveWindowLeft()
{
    Gtk::SpinButton *translate_rate;
    this->builder->get_widget("translate_rate", translate_rate);

    this->window->move(-translate_rate->get_value(), sgi::HORIZONTAL);
    this->redrawObjects();
}

void MainWindow::moveWindowRight()
{
    Gtk::SpinButton *translate_rate;
    this->builder->get_widget("translate_rate", translate_rate);

    this->window->move(translate_rate->get_value(), sgi::HORIZONTAL);
    this->redrawObjects();
}	 	  	 	   	      	    		  	     	       	 	

void MainWindow::moveObject()
{
    Gtk::TreeView* objects_tree = nullptr;
    builder->get_widget("objects_tree_view", objects_tree); // refBuilder is Gtk::Builder instance

    Glib::RefPtr<Gtk::TreeSelection> selection = objects_tree->get_selection();
    Gtk::TreeModel::iterator selected_row = selection->get_selected();

    if(selected_row) //If anything is selected
    {
        Gtk::TreeModel::Row row = *selected_row;
        ObjectNameColumn columns;

        std::string *name = new std::string(row.get_value(columns.object_name_column));

        Gtk::SpinButton *translate_x;
        this->builder->get_widget("translate_x", translate_x);
        Gtk::SpinButton *translate_y;
        this->builder->get_widget("translate_y", translate_y);
        Gtk::SpinButton *translate_z;
        this->builder->get_widget("translate_z", translate_z);

        Coordinate *point = new Coordinate(translate_x->get_value(),
                                           translate_y->get_value(),
                                           translate_z->get_value());

        Transform::getInstance()->translateObject(this->display_file->getObject(*name), point);
        this->redrawObjects();
    }
    else
    {
        Gtk::MessageDialog dlg("No object selected.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        dlg.set_title("Error");
        dlg.run();
    }	 	  	 	   	      	    		  	     	       	 	
}

void MainWindow::scaleObject()
{
    Gtk::TreeView* objects_tree = nullptr;
    builder->get_widget("objects_tree_view", objects_tree); // refBuilder is Gtk::Builder instance

    Glib::RefPtr<Gtk::TreeSelection> selection = objects_tree->get_selection();
    Gtk::TreeModel::iterator selected_row = selection->get_selected();

    if(selected_row) //If anything is selected
    {
        Gtk::TreeModel::Row row = *selected_row;
        ObjectNameColumn columns;

        std::string *name = new std::string(row.get_value(columns.object_name_column));

        Gtk::SpinButton *scale_x;
        this->builder->get_widget("scale_x", scale_x);
        Gtk::SpinButton *scale_y;
        this->builder->get_widget("scale_y", scale_y);
        Gtk::SpinButton *scale_z;
        this->builder->get_widget("scale_z", scale_z);

        Coordinate *scale = new Coordinate(scale_x->get_value(),
                                           scale_y->get_value(),
                                           scale_z->get_value());

        Transform::getInstance()->scaleObjectGeometricCenter(this->display_file->getObject(*name), scale);
        this->redrawObjects();
    }
    else
    {
        Gtk::MessageDialog dlg("No object selected.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        dlg.set_title("Error");
        dlg.run();
    }	 	  	 	   	      	    		  	     	       	 	
}

void MainWindow::rotateObject()
{
    Gtk::TreeView* objects_tree = nullptr;
    builder->get_widget("objects_tree_view", objects_tree); // refBuilder is Gtk::Builder instance

    Glib::RefPtr<Gtk::TreeSelection> selection = objects_tree->get_selection();
    Gtk::TreeModel::iterator selected_row = selection->get_selected();

    if(selected_row) //If anything is selected
    {
        Gtk::TreeModel::Row row = *selected_row;
        ObjectNameColumn columns;

        std::string *name = new std::string(row.get_value(columns.object_name_column));

        Gtk::SpinButton *rotate_angle;
        this->builder->get_widget("rotate_angle", rotate_angle);

        Gtk::RadioButton *origin_rotate_radio;
        builder->get_widget("origin_rotate_radio", origin_rotate_radio);

        Gtk::RadioButton *center_rotate_radio;
        builder->get_widget("center_rotate_radio", center_rotate_radio);

        Gtk::RadioButton *point_rotate_radio;
        builder->get_widget("point_rotate_radio", point_rotate_radio);

        GraphicObject *object = this->display_file->getObject(*name);

        if (origin_rotate_radio->get_active())
        {
            Coordinate *origin = new Coordinate(0.0, 0.0, 0.0);
            Transform::getInstance()->rotateObject(object, origin, rotate_angle->get_value());
        }	 	  	 	   	      	    		  	     	       	 	
        else if(center_rotate_radio->get_active())
        {
            Transform::getInstance()->rotateObject(object, object->getGeometricCenter(), rotate_angle->get_value());
        }
        else if(point_rotate_radio->get_active())
        {
            Gtk::SpinButton *rotate_x;
            this->builder->get_widget("rotate_x", rotate_x);
            Gtk::SpinButton *rotate_y;
            this->builder->get_widget("rotate_y", rotate_y);
            Gtk::SpinButton *rotate_z;
            this->builder->get_widget("rotate_z", rotate_z);

            Coordinate *point = new Coordinate(rotate_x->get_value(),
                                               rotate_y->get_value(),
                                               rotate_z->get_value());

            Transform::getInstance()->rotateObject(object, point, rotate_angle->get_value());

        }

        this->redrawObjects();
    }
    else
    {
        Gtk::MessageDialog dlg("No object selected.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        dlg.set_title("Error");
        dlg.run();
    }

}
	 	  	 	   	      	    		  	     	       	 	
