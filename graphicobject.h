#ifndef GRAPHICOBJECT_H
#define GRAPHICOBJECT_H


#include "coordinate.h"
#include <string>
#include <list>
#include <gdkmm/color.h>

enum GraphicObjectType
{
    POINT,
    LINE,
    POLYGON,
    BEZIER,
    BSPLINE
};

class GraphicObject
{
public:
    GraphicObject(std::string*, GraphicObjectType);
    virtual ~GraphicObject();
    GraphicObjectType getType();
    std::string getName();
    virtual std::list<Coordinate*> getWorldCoordinates();
    virtual std::list<Coordinate*> getNormalizedCoordinates() = 0;
    virtual void setNormalizedCoordinates(std::list<Coordinate*> coordinates) = 0;
    virtual GraphicObject *clone();
    Coordinate *getGeometricCenter();
    std::string toObj();
    void setName(std::string* name);
    void setStrokeColor(Gdk::Color);
    Gdk::Color getStrokeColor();

protected:
    std::string *name;
    GraphicObjectType type;
    Gdk::Color strokeColor;
};

#endif // GRAPHICOBJECT_H
	 	  	 	   	      	    		  	     	       	 	
