#include "addobjectsdialog.h"


AddObjectsDialog::AddObjectsDialog(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder)
: Gtk::Dialog(cobject),
  builder(builder)
{
//    //Get the Glade-instantiated Button, and connect a signal handler:
//    m_refGlade->get_widget("quit_button", m_pButton);
//    if(m_pButton)
//    {
//        m_pButton->signal_clicked().connect( sigc::mem_fun(*this, &DerivedDialog::on_button_quit) );
//    }

//    Glib::RefPtr<Gtk::Builder> builder;
//    builder = Gtk::Builder::create_from_file("add_object_dialog.glade");
//    builder->get_widget("add_object_dialog", this);


//    Gtk::Button* button_add_point = nullptr;
//    builder->get_widget("button_add_point", button_add_point);
//    button_add_point->signal_clicked().connect(sigc::mem_fun(*this, &AddObjectsDialog::addPoint));

//    Gtk::Button* button_add_line = nullptr;
//    builder->get_widget("button_add_line", button_add_line);
//    button_add_line->signal_clicked().connect(sigc::mem_fun(*this, &AddObjectsDialog::addLine));

//    Gtk::Button* button_add_polygon = nullptr;
//    builder->get_widget("button_add_polygon", button_add_polygon);
//    button_add_polygon->signal_clicked().connect(sigc::mem_fun(*this, &AddObjectsDialog::addPolygon));

//    Gtk::Button* button_add_polygon_point = nullptr;
//    builder->get_widget("button_add_polygon_point", button_add_polygon_point);
//    button_add_polygon_point->signal_clicked().connect(sigc::mem_fun(*this, &AddObjectsDialog::addPolygonPoint));

//    Gtk::Button* button_delete_polygon_point = nullptr;
//    builder->get_widget("button_delete_polygon_point", button_delete_polygon_point);
//    button_delete_polygon_point->signal_clicked().connect(sigc::mem_fun(*this, &AddObjectsDialog::deletePolygonPoint));

}	 	  	 	   	      	    		  	     	       	 	

AddObjectsDialog::~AddObjectsDialog()
{

}

void AddObjectsDialog::addPoint()
{

    Gtk::Entry *point_name;
    this->builder->get_widget("point_name", point_name);

    if(point_name->get_text().compare("") == 0)
    {
        Gtk::MessageDialog dlg("Missing required field 'Name'.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
        dlg.set_title("Error");
        dlg.run();
    }
    else
    {

    }


//    if(point_name.get_text().length() == 0) {
//        QMessageBox::warning(this,"Error","Missing required field 'Name'.",QMessageBox::Ok);
//    } else {
//        emit signalAddPoint(name, ui->pointX->value(), ui->pointY->value(), ui->pointZ->value());
//    }
}

void AddObjectsDialog::addLine()
{
//    QString *name = new QString(this->ui->lineName->text());
//    if(name->isEmpty()) {
//        QMessageBox::warning(this,"Error","Missing required field 'Name'.",QMessageBox::Ok);
//    } else {	 	  	 	   	      	    		  	     	       	 	
//        emit signalAddLine(name, ui->lineX1->value(), ui->lineY1->value(), ui->lineZ1->value(),
//                           ui->lineX2->value(), ui->lineY2->value(), ui->lineZ2->value());
//    }
}


void AddObjectsDialog::addPolygon()
{
//    QString *name = new QString(this->ui->polygonName->text());
//    if(name->isEmpty()) {
//        QMessageBox::warning(this,"Error","Missing required field 'Name'.",QMessageBox::Ok);
//    } else if (this->ui->polygonPoints->rowCount() < 3) {
//        QMessageBox::warning(this,"Error","Required at least 3 points.",QMessageBox::Ok);
//    } else {
//        emit signalAddPolygon(name, this->ui->polygonPoints);
//    }
}

void AddObjectsDialog::addPolygonPoint()
{
//    int total = this->ui->polygonPoints->rowCount();
//    this->ui->polygonPoints->insertRow(total);
//    this->ui->polygonPoints->setItem(total, 0, new QTableWidgetItem(QString::number(this->ui->polygonX->value())));
//    this->ui->polygonPoints->setItem(total, 1, new QTableWidgetItem(QString::number(this->ui->polygonY->value())));
//    this->ui->polygonPoints->setItem(total, 2, new QTableWidgetItem(QString::number(this->ui->polygonZ->value())));
}

void AddObjectsDialog::deletePolygonPoint()
{
//    this->ui->polygonPoints->removeRow(this->ui->polygonPoints->currentRow());
}



	 	  	 	   	      	    		  	     	       	 	
